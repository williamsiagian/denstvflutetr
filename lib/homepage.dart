import 'package:dens_tv_project/login.dart';
import 'package:dens_tv_project/youtube.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  final String username;
  final String password;

  HomePage({Key key, this.username, this.password}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Image from assets"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 15.0,
              ),
              Text("JUSTICE LEAGUE",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  )),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                  onTap: () {
                    checkVideo("A");
                  }, // handle your image tap here
                  child: Image.asset('assets/justice_league.png')
              ),
              SizedBox(
                height: 15.0,
              ),
              Text("THE AVENGERS",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  )),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                onTap: () {
                  checkVideo("B");
                }, // handle your image tap here
                child: Image.asset('the_avengers.png'),
              ),
              SizedBox(
                height: 15.0,
              ),
              Text("JURRASIC PARK",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 14
                  )),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                  onTap: () {
                    checkVideo("C");
                  }, // handle your image tap here
                  child: Image.asset('assets/jurassic_park.png')
              ),
            ],
          ),
        ), //   <--- image
      ),
    );
  }

  void checkVideo(String video){
    if(video == "A"){
      if(widget.username == "user1@gmail.com" && widget.password == "password123"){
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => YoutubeViewPage(
                  title: "JUSTICE LEAGUE",
                  url: "https://www.youtube.com/watch?v=vM-Bja2Gy04",
                )));
      }else{
        showDialogAccess();
      }
    }else if(video == "B"){
      if(widget.username == "user2@gmail.com" && widget.password == "password321"){
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => YoutubeViewPage(
                  title: "THE AVENGERS",
                  url: "https://www.youtube.com/watch?v=eOrNdBpGMv8",
                )));
      }else{
        showDialogAccess();
      }
    }else{
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => YoutubeViewPage(
                title: "JURRASIC PARK",
                url: "https://www.youtube.com/watch?v=lc0UehYemQA",
              )));
    }
  }
  
  Widget showDialogAccess(){
    showDialog(
      builder: (context) => AlertDialog(
        title: Text('Dont have access to watch this video'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginPage()));
            },
            child: Text('LOGOUT'),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text('CLOSE'),
          )
        ],
      ),
      context: context,
    );
  }
}
