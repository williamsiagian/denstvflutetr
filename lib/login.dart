import 'package:dens_tv_project/homepage.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {

  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _showPassword = false;
  final _formLoginKey = GlobalKey<FormState>();

  final _username = TextEditingController();
  final _password = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        height: height,
        width: width,
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formLoginKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: width,
                  height: height * 0.40,
                  child: Image.asset(
                    'assets/dens_logo.png',
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Login',
                        style: TextStyle(
                            fontSize: 25.0, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'Username',
                    suffixIcon: Icon(Icons.email),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  controller: _username,
                  validator: (value) {
                    return value.isEmpty ? 'Username can\'t be empty' : null;
                  },
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  obscureText: !this._showPassword,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    suffixIcon: IconButton(
                        icon: this._showPassword
                            ? Icon(Icons.visibility)
                            : Icon(Icons.visibility_off),
                        onPressed: () {
                          setState(
                                  () => this._showPassword = !this._showPassword);
                        }),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  controller: _password,
                  validator: (value) {
                    return value.isEmpty ? 'Password can\'t be empty' : null;
                  },
                ),
                SizedBox(
                  height: 30.0,
                ),
                RaisedButton(
                  child: Text(
                    'SIGN IN',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  padding: EdgeInsets.symmetric(
                    vertical: 15.0,
                  ),
                  onPressed: () {
                    String username = _username.text;
                    String password = _password.text;
                    if (_username.text.isNotEmpty || _password.text.isNotEmpty) {
                      if ((username == "user1@gmail.com" && password == "password123") || (username == "user2@gmail.com" && password == "password321")){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomePage(
                                  username: username,
                                  password: password,
                                )));
                      }else{
                        showDialog(
                          builder: (context) => AlertDialog(
                            title: Text('Wrong Username or Password'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text('Retry'),
                              )
                            ],
                          ),
                          context: context,
                        );
                      }
                    } else {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: Text('Username or Password Cannot be blank'),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Retry'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    }
                  },
                ),
                SizedBox(height: 20.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
