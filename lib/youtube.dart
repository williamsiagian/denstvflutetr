import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class YoutubeViewPage extends StatefulWidget {
  final String title;
  final String url;

  YoutubeViewPage({Key key, this.title, this.url}) : super(key: key);

  @override
  _YoutubeViewPageState createState() => _YoutubeViewPageState();
}

class _YoutubeViewPageState extends State<YoutubeViewPage> {
  YoutubePlayerController _controller;

  void runYoutubePlayer(){
    _controller = YoutubePlayerController(initialVideoId: YoutubePlayer.convertUrlToId(widget.url),
    flags: YoutubePlayerFlags(
      enableCaption: false,
      isLive: false,
      autoPlay: true,

    ));
  }

  @override
  void initState() {
    runYoutubePlayer();
    super.initState();
  }

  @override
  void deactivate() {
    _controller.pause();
    super.deactivate();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return YoutubePlayerBuilder(
      player: YoutubePlayer(
        controller: _controller,
      ),
      builder: (context, player){
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: Center(
            child: player,
          )
        );
      },
    );
  }
}
