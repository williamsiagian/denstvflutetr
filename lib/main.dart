import 'package:flutter/material.dart';
import 'login.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Youtube Player Dens.TV',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(primarySwatch: Colors.blue, fontFamily: 'Nunito'),
        home: new LoginPage());
  }
}
